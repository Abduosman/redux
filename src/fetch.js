import actions from './action'
import axios from 'axios'

function fetching(){
	console.log('action')
	return function(dispatch){
		axios.get('https://jsonplaceholder.typicode.com/posts')
			.then(function(re){
			dispatch(actions(re.data))
		})
}}
	
export default fetching