
export default function reducers(state =  {}, action){
		switch(action.type){
			case 'FETCH':
				return {...state, fetch: action.payload}
			default:
				return state
		}
}
