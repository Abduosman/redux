import React, { Component } from 'react';
import fetching from './fetch'
import {connect} from 'react-redux'
import './index.css'

class App extends Component {

	componentWillMount(){
		this.props.fetching()
	}
  render() {
const { fetch } = this.props;
    return (
      <React.Fragment>
			<div className='header'>Redux App </div>
		      {
						fetch ? Object.values(fetch).map(function(item, i){
							return( <div key={i} className="container">
							 {item.map((e)=>{
								 	return (<li key={e.id} className="list-item">
													{e.userId}{e.title} {' '}{e.body}
												</li>)
									}
							)}
</div>)
					}): {}
					}
      </React.Fragment>
    );
  }
}



function mapStateToProps (state){
	return {
		fetch: state.reducers
	}
}

export default connect(mapStateToProps, { fetching })(App);
